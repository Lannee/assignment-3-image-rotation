#include "bmp_file.h"
#include "error_codes.h"
#include "image.h"
#include "rotate.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARGUMENTS_AMOUNT 4
#define SOURCE_FILE_INDEX 1
#define TARGET_FILE_INDEX 2
#define ANGLE_INDEX 3

void error_exit(char* message, enum error_codes exit_code);

int main( int argc, char** argv ) {
    if(argc != ARGUMENTS_AMOUNT) {
        error_exit("Invalid number of arguments!", INVAID_PARAMETR_CODE);
    }

    long angle = strtol(argv[ANGLE_INDEX], NULL, 10);
    if(!angle && strcmp(argv[ANGLE_INDEX],"0") != 0) {
        error_exit("Invalid value for angle", INVAID_PARAMETR_CODE);
    }

    FILE* source_file = fopen(argv[SOURCE_FILE_INDEX], "rb");
    FILE* target_file = fopen(argv[TARGET_FILE_INDEX], "wb");
    if(!source_file) error_exit("Cannot open source file!", OPEN_FILE_ERROR_CODE);
    if(!target_file) {
        fclose(source_file);
        error_exit("Target file cannot be opened or created!", OPEN_FILE_ERROR_CODE);
    }

    struct image image;

    enum read_status readStatus = from_bmp(source_file, &image);
    if(readStatus != READ_OK) {
        fclose(source_file);
        fclose(target_file);
    }
    switch (readStatus) {
        case READ_INVALID_FORMAT:
            error_exit("Read invalid format", READ_ERROR_CODE);
        case READ_INVALID_HEADER:
            error_exit("Invalid header!", READ_ERROR_CODE);
        case READ_INVALID_DATA:
            error_exit("Invalid data!", READ_ERROR_CODE);
        case READ_OK: {}
    }
    fclose(source_file);

    switch (rotate(&image, angle)) {
        case UNSUPPORTED_ANGLE:
            fclose(target_file);
            error_exit("Invalid angle!", INVALID_VALUE_CODE);
        case ROTATE_OK: {}
    }

    switch (to_bmp(target_file, &image)) {
        case WRITE_ERROR:
            fclose(target_file);
            error_exit("Cannot write to target file!", WRITE_ERROR_CODE);
        case WRITE_OK: {}
    }
    fclose(target_file);

    image_free(&image);
    printf("Rotation completed!");
    return 0;
}

void error_exit(char* message, enum error_codes exit_code) {
    fprintf(stderr, "%s", message);
    exit(exit_code);
}
