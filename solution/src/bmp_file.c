#include "bmp_file.h"
#include "error_codes.h"

#include <stdlib.h>

#define BMP_FILE_CODE 0x4D42

#define HEADER_SIZE 40
#define HEADER_PLANES 1
#define HEADER_BIT_COUNT 24
#define HEADER_COMPRESSION 0
#define HEADER_X_PELS_PER_METERS 2834
#define HEADER_Y_PELS_PER_METERS 2834
#define HEADER_CLR_USED 0
#define HEADER_CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static uint8_t get_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header bmpHeader;
    if(fread(&bmpHeader, sizeof(struct bmp_header), 1, in) == 0) {
        return READ_INVALID_HEADER;
    }

    if(bmpHeader.bfType != BMP_FILE_CODE) {   // a.k. compare bfType with 'BM'
        return READ_INVALID_FORMAT;
    }

    fseek(in, bmpHeader.bOffBits, SEEK_SET);
    int8_t* image_data = malloc(bmpHeader.biSizeImage);
    if(fread(image_data, bmpHeader.biSizeImage, 1, in) < 1) {
        free(image_data);
        return READ_INVALID_DATA;
    }
    *img = image_new(bmpHeader.biWidth, bmpHeader.biHeight);

    uint8_t byte_padding = get_padding(bmpHeader.biWidth);

    for (uint32_t i = 0; i < bmpHeader.biHeight; ++i) {
        for (uint32_t j = 0; j < bmpHeader.biWidth; ++j) {
            int8_t* pixel_start = image_data + i * (sizeof(struct pixel) * bmpHeader.biWidth + byte_padding) + j * sizeof(struct pixel);
            img->data[i * bmpHeader.biWidth + j] =
                    pixel_new(*(pixel_start + 2), *(pixel_start + 1), *pixel_start);
        }
    }

    free(image_data);
    return READ_OK;
}

static struct bmp_header gen_header(struct image const* img) {
    uint32_t biSizeImage = (img->width * sizeof(struct pixel) + get_padding(img->width)) * img->height;
    return (struct bmp_header) {.bfType = BMP_FILE_CODE,
            .bfileSize = biSizeImage + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = HEADER_PLANES,
            .biBitCount = HEADER_BIT_COUNT,
            .biCompression = HEADER_COMPRESSION,
            .biSizeImage = biSizeImage,
            .biXPelsPerMeter = HEADER_X_PELS_PER_METERS,
            .biYPelsPerMeter = HEADER_Y_PELS_PER_METERS,
            .biClrUsed = HEADER_CLR_USED,
            .biClrImportant = HEADER_CLR_IMPORTANT};
}


enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = gen_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) {
        return WRITE_ERROR;
    }

    int8_t* image_data = malloc(header.biSizeImage);
    if(!image_data) exit(MEMMORY_LACK_CODE);

    uint8_t byte_padding = get_padding(img->width);
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            int8_t* pixel_start = image_data +  i * (sizeof(struct pixel) * header.biWidth + byte_padding) + j * sizeof(struct pixel);
            *pixel_start = img->data[img->width * i + j].b;
            *(pixel_start + 1) = img->data[img->width * i + j].g;
            *(pixel_start + 2) = img->data[img->width * i + j].r;
        }
        for (uint64_t j = 0; j < byte_padding; ++j) {
            image_data[(img->width * 3 + byte_padding) * i + img->width * 3 + j] = 0;
        }
    }

    if (fwrite(image_data, header.biSizeImage, 1, out) < 1) {
        free(image_data);
        return WRITE_ERROR;
    }
    free(image_data);
    return WRITE_OK;
}
