#include "rotate.h"

static enum rotate_status rotate_270(struct image* image) {
    struct image copy = image_new(image->height, image->width);
    for (uint64_t i = 0; i < image->height; ++i) {
        for (uint64_t j = 0; j < image->width; ++j) {
            copy.data[j * copy.width + (image->height - 1 - i)] = image->data[i * image->width + j];
        }
    }
    image_free(image);
    *image = copy;
    return ROTATE_OK;
}

static enum rotate_status rotate_180(struct image* image) {
    struct image copy = image_new(image->width, image->height);
    for (uint64_t i = 0; i < copy.height; ++i) {
        for (uint64_t j = 0; j < copy.width; ++j) {
            copy.data[copy.width * copy.height - 1 - (i * copy.width + j)] = image->data[i * image->width + j];
        }
    }
    image_free(image);
    *image = copy;
    return ROTATE_OK;
}

static enum rotate_status rotate_90(struct image* image) {
    struct image copy = image_new(image->height, image->width);
    for (uint64_t i = 0; i < copy.height; ++i) {
        for (uint64_t j = 0; j < copy.width; ++j) {
            copy.data[i * copy.width + j] = image->data[j * image->width + (copy.height - 1 - i)];
        }
    }
    image_free(image);
    *image = copy;
    return ROTATE_OK;
}

enum rotate_status rotate(struct image* image, int64_t angle) {
    angle = angle % 360;

    switch (angle) {
        case 0 :
            return ROTATE_OK;
        case 90:
        case -270 :
            return rotate_90(image);
        case 180 :
        case -180 :
            return rotate_180(image);
        case 270 :
        case -90 :
            return rotate_270(image);
        default:
            return UNSUPPORTED_ANGLE;
    }
}
