#include "image.h"
#include "error_codes.h"

#include <stdlib.h>

struct pixel pixel_new(int8_t r, int8_t g, int8_t b) {
    return (struct pixel) {r, g, b};
}

struct image image_new(const uint64_t width, const uint64_t height) {
    struct pixel* data = malloc(width * height * sizeof(struct pixel));
    if(!data) exit(MEMMORY_LACK_CODE);

    return (struct image) {
        .width = width,
        .height = height,
        .data = data
    };
}

void image_free(struct image* image) {
    if(image->data)
        free(image->data);
}
