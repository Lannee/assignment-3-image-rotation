//
// Created by uncle on 11.11.2023.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_FILE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_FILE_H

#include "image.h"

#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_FORMAT,
    READ_INVALID_DATA
};

enum read_status from_bmp( FILE* in, struct image* img );


enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
};

enum write_status to_bmp( FILE* out, struct image const* img );


#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_FILE_H
