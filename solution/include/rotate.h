//
// Created by uncle on 11.11.2023.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H

#include "image.h"
#include <stdint.h>

enum rotate_status {
    ROTATE_OK = 0,
    UNSUPPORTED_ANGLE,
};

// rotates image by custom angle
// gets image to rotate and an angle
// returns rotate status
enum rotate_status rotate(struct image* image, int64_t angle);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
