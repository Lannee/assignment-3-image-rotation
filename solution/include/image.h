#include <stdint.h>

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#pragma pack(push, 1)
struct pixel {
    int8_t r, g, b;
};
#pragma pack(pop)

struct pixel pixel_new(int8_t r, int8_t g, int8_t b);


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_new(uint64_t width, uint64_t height);

void image_free(struct image* image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
